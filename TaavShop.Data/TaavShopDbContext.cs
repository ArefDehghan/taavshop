﻿using Microsoft.EntityFrameworkCore;
using TaavShop.Core.Models;

namespace TaavShop.Data
{
    public class TaavShopDbContext : DbContext
    {
        public TaavShopDbContext(DbContextOptions<TaavShopDbContext> options)
            : base (options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Product>()
                .HasIndex(t => t.Title)
                .IsUnique();
        }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<ShowcaseProduct> ShowcaseProducts { get; set; }
    }
}
