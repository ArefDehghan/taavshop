﻿using TaavShop.Core.Models;
using TaavShop.Core.Repositories;

namespace TaavShop.Data.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(TaavShopDbContext context) 
            : base(context)
        {
        }

        public TaavShopDbContext TaavShopDbContext =>
            Context as TaavShopDbContext;
    }
}
