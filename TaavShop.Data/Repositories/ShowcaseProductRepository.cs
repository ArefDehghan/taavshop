﻿using TaavShop.Core.Models;
using TaavShop.Core.Repositories;

namespace TaavShop.Data.Repositories
{
    public class ShowcaseProductRepository : Repository<ShowcaseProduct>, IShowcaseProductRepository
    {
        public ShowcaseProductRepository(TaavShopDbContext context) : base(context)
        {
        }

        public TaavShopDbContext TaavShopDbContext =>
            Context as TaavShopDbContext;
    }

}
