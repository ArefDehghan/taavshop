﻿using TaavShop.Core.Models;
using TaavShop.Core.Repositories;

namespace TaavShop.Data.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(TaavShopDbContext context)
            : base(context)
        {
        }

        public TaavShopDbContext TaavShopDbContext =>
            Context as TaavShopDbContext;
    }
}
