﻿using TaavShop.Core;
using TaavShop.Core.Repositories;

namespace TaavShop.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly TaavShopDbContext _context;

        public UnitOfWork(TaavShopDbContext context, IProductRepository productRepository, IOrderRepository orderRepository, IShowcaseProductRepository showcaseProductRepository)
        {
            _context = context;

            Products = productRepository;
            Orders = orderRepository;
            ShowcaseProducts = showcaseProductRepository;
        }

        public IProductRepository Products { get; private set; }

        public IOrderRepository Orders { get; private set; }

        public IShowcaseProductRepository ShowcaseProducts { get; private set; }

        public int Complete() => 
            _context.SaveChanges();

        public void Dispose() => 
            _context.Dispose();

    }
}
