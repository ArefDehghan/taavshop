﻿using System;
using TaavShop.Core.Repositories;

namespace TaavShop.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; }

        IOrderRepository Orders { get; }

        IShowcaseProductRepository ShowcaseProducts { get; }

        int Complete();
    }
}
