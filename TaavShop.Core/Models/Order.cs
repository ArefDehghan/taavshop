﻿using System.ComponentModel.DataAnnotations;

namespace TaavShop.Core.Models
{
    public class Order
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int Number { get; set; }

        public int Price { get; set; }
    }
}
