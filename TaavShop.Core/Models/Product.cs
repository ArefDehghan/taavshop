﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaavShop.Core.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public int Price { get; set; }

        public int Stock { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return $"Id: {Id} | Title: {Title} | Price: {Price} | Stock: {Stock} | Description: {Description}";
        }
    }
}
