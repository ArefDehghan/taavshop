﻿using System.ComponentModel.DataAnnotations;

namespace TaavShop.Core.Models
{
    public class ShowcaseProduct
    {
        public int Id { get; set; }

        public int ProductId { get; set; }
    }
}
