﻿using TaavShop.Core.Models;

namespace TaavShop.Core.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
