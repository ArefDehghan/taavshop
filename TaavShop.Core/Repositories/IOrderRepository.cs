﻿using TaavShop.Core.Models;

namespace TaavShop.Core.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
