﻿using TaavShop.Core.Models;

namespace TaavShop.Core.Repositories
{
    public interface IShowcaseProductRepository : IRepository<ShowcaseProduct>
    {
    }
}
