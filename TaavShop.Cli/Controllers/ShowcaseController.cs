﻿using System.Text;
using TaavShop.Core;
using TaavShop.Core.Models;

namespace TaavShop.Cli.Controllers
{
    public class ShowcaseController
    {
        private readonly IUnitOfWork _context;

        public ShowcaseController(IUnitOfWork context)
        {
            _context = context;
        }

        public string ExecuteCommands(string[] args)
        {
            if (args.Length == 2)
            {
                switch (args[1])
                {
                    case "list":
                        return List();

                    default:
                        return "Error: Command not found.";
                }
            }

            else if (args.Length == 3)
            {
                var isIdValid = int.TryParse(args[2], out int id);
                if (!isIdValid)
                    return "Error: Id must be a number.";

                switch (args[1])
                {
                    case "add":
                        return Add(id);

                    case "delete":
                        return Delete(id);

                    default:
                        return "Error: Command not found.";
                }
            }

            return "Error: Command not found.";
        }

        private string Add(int id)
        {
            var product = _context.Products.SingleOrDefault(p => p.Id == id);

            if (!ProductExist(id))
                return "Error: Product not found.";

            if (ShowcaseProductExist(id))
                return "Error: Product is already in showcase.";

            _context.ShowcaseProducts.Add(new ShowcaseProduct { ProductId = id });

            foreach (var item in _context.ShowcaseProducts.GetAll())
            {
                System.Console.WriteLine(item.Id);
            }

            var entries = _context.Complete();

            if (entries > 0)
                return "Product added to showcase.";
                
            return "Error: Failed to add product to showcase.";
        }

        private string Delete(int id)
        {
            var showcaseProduct = _context.ShowcaseProducts.SingleOrDefault(s => s.ProductId == id);

            if (!ShowcaseProductExist(id))
                return "Error: Showcase product not found.";

            _context.ShowcaseProducts.Remove(showcaseProduct);
            var entries = _context.Complete();

            if (entries > 0)
                return "Showcase product deleted.";

            return "Error: Failed to delete showcase product.";
        }

        private string List()
        {
            var showcaseProducts = _context.ShowcaseProducts.GetAll();
            var stringBuilder = new StringBuilder();

            foreach (var showcaseProduct in showcaseProducts)
            {
                var product = _context.Products.SingleOrDefault(p => p.Id == showcaseProduct.ProductId);
                stringBuilder.AppendLine(product.ToString());
            }

            return stringBuilder.ToString();
        }

        private bool ProductExist(int productId)
        {
            var existingProduct = _context.Products.SingleOrDefault(p => p.Id == productId);

            return existingProduct != null;
        }

        private bool ShowcaseProductExist(int showcaseProductId)
        {
            var existingShowcaseProduct = _context.ShowcaseProducts.SingleOrDefault(p => p.ProductId == showcaseProductId);

            return existingShowcaseProduct != null;
        }
    }
}
