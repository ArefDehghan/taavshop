﻿using System;
using System.Text;
using TaavShop.Core;
using TaavShop.Core.Models;

namespace TaavShop.Cli.Controllers
{
    public class ProductController
    {
        private readonly IUnitOfWork _context;

        public ProductController(IUnitOfWork context)
        {
            _context = context;
        }

        public string ExecuteCommands(string[] args)
        {
            if (args.Length == 2)
            {
                switch (args[1])
                {
                    case "list":
                        return List();

                    case "new":
                        return New();

                    default:
                        return "Error: Command not found.";
                }
            }

            else if (args.Length == 3)
            {
                var isIdValid = int.TryParse(args[2], out int id);
                if (!isIdValid)
                    return "Error: Id must be a number.";

                switch (args[1])
                {
                    case "edit":
                        return Edit(id);

                    case "delete":
                        return Delete(id);

                    case "view":
                        return View(id);

                    default:
                        return "Error: Command not found.";
                }
            }

            return "Error: Command not found.";
        }

        private string List()
        {
            var products = _context.Products.GetAll();
            var stringBuilder = new StringBuilder();

            foreach (var product in products)
                stringBuilder.AppendLine(product.ToString());    

            return stringBuilder.ToString();
        }

        private string New()
        {
            var product = new Product();
            GetProduct(product);

            if (!IsProductValid(product))
            {
                return "Error: Input is not valid:\n" +
                    "\t1. Price and Stock must be set to a integer value greater than 0.\n" +
                    "\t2. Title and Description can't be empty (or white space).";
            }

            if (ProductExist(product.Id) | ProductExist(product.Title))
                return "Error: Product already exist.";

            _context.Products.Add(product);
            var entries = _context.Complete();

            if (entries > 0)
                return $"{product.Title} added successfully.";

            return $"Error: Failed to add the product.";
        }

        private string Edit(int id)
        {
            var product = _context.Products.SingleOrDefault(p => p.Id == id);

            if (!ProductExist(id))
                return "Error: Product not found.";

            GetProduct(product);

            if (!IsProductValid(product))
            {
                return "Error: Input is not valid:\n" +
                    "\t1. Price and Stock must be set to a integer value greater than 0.\n" +
                    "\t2. Title and Description can't be empty (or white space).";
            }

            var entries = _context.Complete();

            if (entries > 0)
                return $"{product.Title} edited successfully.";

            return $"Error: Failed to edit the product.";
        }

        private string Delete(int id)
        {
            var product = _context.Products.SingleOrDefault(p => p.Id == id);

            if (!ProductExist(id))
                return "Error: Product not found.";

            var orderedProduct = _context.Orders.SingleOrDefault(o => o.ProductId == id);

            if (orderedProduct != null)
                return "Error: You can't delete a product that is in the basket.";

            Console.Write($"You are about to delete {product.Title}. Are you sure? [Y/N]:");
            var key = Console.ReadKey();

            if (key.KeyChar == 'y')
            {
                _context.Products.Remove(product);
                var entries = _context.Complete();

                if (entries > 0)
                    return $"\n{product.Title} deleted.";
            }
            if (key.KeyChar == 'n')
                return "\nDelete operation cancelled.";

            return "\nFailed to delete the product.";
        }

        private string View(int id)
        {
            var product = _context.Products.SingleOrDefault(p => p.Id == id);

            if (!ProductExist(id))
                return "Error: Product not found.";

            return product.ToString();
        }

        private void GetProduct(Product product)
        {
            Console.Write("Title: ");
            product.Title = Console.ReadLine();

            Console.Write("Price: ");
            var isPriceValid = int.TryParse(Console.ReadLine(), out int price);
            if (isPriceValid)
                product.Price = price;

            Console.Write("Stock: ");
            var isStockValid = int.TryParse(Console.ReadLine(), out int stock);
            if (isStockValid)
                product.Stock = stock;

            Console.Write("Description: ");
            product.Description = Console.ReadLine();
        }

        private bool IsProductValid(Product product)
        {
            if (string.IsNullOrWhiteSpace(product.Title) || string.IsNullOrWhiteSpace(product.Description) || product.Price <= 0)
                return false;

            return true;
        }

        private bool ProductExist(int productId)
        {
            var existingProduct = _context.Products.SingleOrDefault(p => p.Id == productId);

            return existingProduct != null;
        }

        private bool ProductExist(string title)
        {
            var product = _context.Products.SingleOrDefault(p => p.Title == title);

            return product != null;
        }
    }
}
