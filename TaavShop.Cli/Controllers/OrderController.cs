﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaavShop.Core;
using TaavShop.Core.Models;

namespace TaavShop.Cli.Controllers
{
    public class OrderController
    {
        private readonly IUnitOfWork _context;

        public OrderController(IUnitOfWork context)
        {
            _context = context;
        }

        public string ExecuteCommands(string[] args)
        {
            if (args.Length == 2)
            {
                switch (args[1])
                {
                    case "list":
                        return List();

                    default:
                        return "Error: Command not found.";
                }
            }

            else if (args.Length == 3)
            {
                if (args[1] == "delete" && args[2] == "all")
                    return DeleteAll();

                var isIdValid = int.TryParse(args[2], out int id);
                if (!isIdValid)
                    return "Error: Id must be a number.";

                switch (args[1])
                {
                    case "add":
                        return Add(id);

                    case "delete":
                        return Delete(id);

                    default:
                        return "Error: Command not found.";
                }
            }

            else if (args.Length == 4)
            {
                var isIdValid = int.TryParse(args[2], out int id);
                if (!isIdValid)
                    return "Error: Id must be a number.";

                var isNumberValid = int.TryParse(args[3], out int number);
                if (!isNumberValid)
                    return "Error: Number must be a number.";

                switch (args[1])
                {
                    case "add":
                        return Add(id, number);

                    default:
                        return "Error: Command not found.";
                }
            }

            return "Error: Command not found.";
        }

        private string List()
        {
            var orders = _context.Orders.GetAll();
            var stringBuilder = new StringBuilder();

            foreach (var order in orders)
            {
                var product = _context.Products.SingleOrDefault(p => p.Id == order.ProductId);
                stringBuilder.AppendLine($"{this.ToString(product, order.Price)} X {order.Number} => Total Price: {order.Price * order.Number}");
            }

            var totalCost = orders.Sum(o => o.Price * o.Number);
                            
            stringBuilder.AppendLine($"\nTotal Cost (%7 Tax Included): {totalCost + (totalCost * 0.07)}");

            return stringBuilder.ToString();
        }

        private string Add(int id, int requestedProductsNumber = 1) 
        {
            var product = _context.Products.SingleOrDefault(p => p.Id == id);

            if (!ProductExist(id))
                return "Error: Product not found.";

            if (IsStockNotEnough(product.Stock, requestedProductsNumber))
                return "Error: Product out of stock.";

            if (OrderExist(id))
            {
                var existingOrder = _context.Orders.SingleOrDefault(o => o.ProductId == product.Id);

                existingOrder.Number += requestedProductsNumber;
                product.Stock -= requestedProductsNumber;
            }
            else
            {
                _context.Orders.Add(new Order { ProductId = product.Id,
                                                Number = requestedProductsNumber,
                                                Price = product.Price});

                product.Stock -= requestedProductsNumber;
            }

            var entries = _context.Complete();

            if (entries > 0)
                return $"Added {requestedProductsNumber} {product.Title}s to basket.";

            return "Error: Failed to add order to the basket.";
        }

        private string Delete(int id)
        {
            var order = _context.Orders.SingleOrDefault(o => o.ProductId == id);

            if (!OrderExist(id))
                return "Error: Order not found.";

            var product = _context.Products.SingleOrDefault(p => p.Id == id);

            order.Number--;
            product.Stock++;

            if (order.Number == 0)
                _context.Orders.Remove(order);
            
            var entries = _context.Complete();

            if (entries > 0)
                return "Order deleted.";

            return "Error: Failed to delete order.";
        }

        private string DeleteAll()
        {
            var orders = _context.Orders.GetAll();

            foreach (var order in orders)
            {
                var product = _context.Products.SingleOrDefault(p => p.Id == order.ProductId);
                product.Stock += order.Number;

                order.Number = 0;
            }

            _context.Orders.RemoveRange(orders);
            var entries = _context.Complete();

            if (entries <= 0)
                return "Error: Failed to clean basket.";

            return "Basket cleaned.";
        }

        private bool ProductExist(int productId)
        {
            var existingProduct = _context.Products.SingleOrDefault(p => p.Id == productId);

            return existingProduct != null;
        }

        private bool OrderExist(int productId)
        {
            var existingProduct = _context.Orders.SingleOrDefault(p => p.ProductId == productId);

            return existingProduct != null;
        }

        private bool IsStockNotEnough(int stock, int requestedProductsNumber) =>
            stock < requestedProductsNumber;

        public string ToString(Product product, int orderPrice)
        {
            return $"Id: {product.Id} | Title: {product.Title} | Price: {orderPrice} | Stock: {product.Stock} | Description: {product.Description}";
        }
    }
}
