﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using TaavShop.Cli.Controllers;
using TaavShop.Core;
using TaavShop.Core.Models;
using TaavShop.Core.Repositories;
using TaavShop.Data;
using TaavShop.Data.Repositories;

namespace TaavShop.Cli
{
    class Program
    {
        const string OrdersFile = @"Orders.json";

        const string ProductsFile = @"Products.json";

        const string ShowcaseProductsFile = @"ShowcaseProducts.json";

        private static IUnitOfWork _unitOfWork;

        static void Main(string[] args)
        {
            var services = new ServiceCollection()
                .AddScoped<ProductController>()
                .AddScoped<OrderController>()
                .AddScoped<ShowcaseController>()
                .AddScoped<IOrderRepository, OrderRepository>()
                .AddScoped<IProductRepository, ProductRepository>()
                .AddScoped<IShowcaseProductRepository, ShowcaseProductRepository>()
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddDbContext<TaavShopDbContext>(options =>
                    options.UseInMemoryDatabase(databaseName: "TaavDb"))
                .BuildServiceProvider();

            _unitOfWork = services.GetService<IUnitOfWork>();

            LoadContext();
            
            var productController = services.GetService<ProductController>();
            var orderController = services.GetService<OrderController>();
            var showcaseController = services.GetService<ShowcaseController>();

            string output;
            switch (args[0])
            {
                case "product":
                    output = productController.ExecuteCommands(args);
                    break;

                case "showcase":
                    output = showcaseController.ExecuteCommands(args);
                    break;

                case "basket":
                    output = orderController.ExecuteCommands(args);
                    break;

                default:
                    output = "Error: Command not found.";
                    break;
            }

            Console.WriteLine(output);

            SaveContext();
        }

        static void LoadContext()
        {
            try
            {
                var products = Read<List<Product>>(ProductsFile);
                _unitOfWork.Products.AddRange(products);

                var orders = Read<List<Order>>(OrdersFile);
                _unitOfWork.Orders.AddRange(orders);

                var ShowcaseProducts = Read<List<ShowcaseProduct>>(ShowcaseProductsFile);
                _unitOfWork.ShowcaseProducts.AddRange(ShowcaseProducts);
            }
            catch (ArgumentNullException)
            {
                SaveContext();
                LoadContext();
            }
            _unitOfWork.Complete();
        }

        static void SaveContext()
        {
            var products = _unitOfWork.Products.GetAll();
            Write(products, ProductsFile);

            var orders = _unitOfWork.Orders.GetAll();
            Write(orders, OrdersFile);

            var showcaseProducts = _unitOfWork.ShowcaseProducts.GetAll();
            Write(showcaseProducts, ShowcaseProductsFile);
        }

        private static T Read<T>(string path) where T : class
        {
            if (string.IsNullOrWhiteSpace(path))
                return null;

            try
            {
                var json = File.ReadAllText(path);
                if (!string.IsNullOrWhiteSpace(json))
                    return JsonSerializer.Deserialize<T>(json);
            }
            catch (FileNotFoundException)
            {
                SaveContext();
            }

            return null;
        }

        private static void Write<T>(T value, string path) where T : class
        {
            var json = JsonSerializer.Serialize(value, value.GetType());
            Debug.Write(json);
            File.WriteAllText(path, json);
        }
    }
}
